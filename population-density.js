const lahore = require("./data/lahore/population_density.json");
const islamabad = require("./data/islamabad/population_density.json");
const karachi = require("./data/karachi/population_density.json");
const ULA_ALL = {
  lahore: [...lahore],
  islamabad: [...islamabad],
  karachi: [...karachi],
};

const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://pmi-admin-user:CRDbFpQDzzP2sSlu@pmi-cluster.cnhta.mongodb.net/pmi-development?retryWrites=true&w=majority"
);
let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async () => {
  let completed = 0;
  let temp_Array = [];
  for (const city in ULA_ALL) {
    const selected_city_locations = ULA_ALL[city];
    console.log(selected_city_locations);
    for (const location of selected_city_locations) {
      let temp_row = {};
      temp_row.city = city;
      temp_row.population = location.population;
      temp_row.polygon = {
        type: "Polygon",
        coordinates: [location.coordinates.map(({ lng, lat }) => [lng, lat])],
      };
      temp_Array.push({ ...temp_row });
    }
    await db.collection("population_density").insertMany([...temp_Array]);
    completed += temp_Array.length;
    temp_Array = [];
    console.log({ completed });
  }
  console.log("Completed");
});
