const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://census-admin-user:cHEKAuGBoDgWrRML@national-census.xbci9.mongodb.net/surveyauto?retryWrites=true&w=majority"
);
let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async () => {
  let completed = 0;
  let temp_Array = [];

  const historyLogInfo = await db
    .collection("historylogs")
    .aggregate([
      //       {
      //         $skip: 5090000,
      //       },
      //   {
      //     $limit: 151200,
      //   },
      {
        $match: {
          action: "Updating_Response",
          updatedAt: { $gte: new Date("2022-07-01T00:00:00.000Z") },
        },
      },
      {
        $project: {
          _id: 0,
          response_id: 1,
          user_id: 1,
          enumerator_id: 1,
          survey_id: 1,
          updatedAt: 1,
        },
      },

      {
        $lookup: {
          from: "users",
          let: { id: "$user_id" },
          pipeline: [
            { $match: { $expr: { $eq: ["$_id", "$$id"] } } },
            { $project: { email: 1, _id: 0 } },
          ],
          as: "qc_user",
        },
      },
      {
        $lookup: {
          from: "enumerators",
          let: { id: "$enumerator_id" },
          pipeline: [
            { $match: { $expr: { $eq: ["$_id", "$$id"] } } },
            { $project: { first_name: 1, last_name: 1, _id: 0 } },
          ],
          as: "enumerator",
        },
      },
      {
        $lookup: {
          from: "surveys",
          let: { id: "$survey_id" },
          pipeline: [
            { $match: { $expr: { $eq: ["$_id", "$$id"] } } },
            { $project: { name: 1, _id: 0 } },
          ],
          as: "survey_name",
        },
      },
      {
        $unwind: {
          path: "$qc_user",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $unwind: {
          path: "$enumerator",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $unwind: {
          path: "$survey_name",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        allowDiskUse: true,
      },
    ])
    .toArray();

  console.log({ completed, historyLogInfo });

  console.log("Completed");
});
