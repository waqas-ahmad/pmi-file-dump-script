const mongoose = require("mongoose");
const fs = require("fs");
mongoose.connect(
  "mongodb+srv://census-admin-user:cHEKAuGBoDgWrRML@national-census.xbci9.mongodb.net/validator?retryWrites=true&w=majority"
);
let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async () => {
  let completed = 0;
  let temp_Array = [];

  const reponse = await db
    .collection("routes")
    .aggregate([
      //       {
      //         $skip: 5090000,
      //       },
      //   {
      //     $limit: 151200,
      //   },
      {
        $match: {
          geo: "LAHORE",
          tenant: "UNILEVER",
        },
      },
      {
        $project: {
          _id: 0,
          route_code: 1,
          distributor_code: 1,
        },
      },

      //   {
      //     $lookup: {
      //       from: "census_shops",
      //       let: { disCode: "$distributor_code", routCode: "$route_code" },
      //       pipeline: [
      //         {
      //           $match: {
      //             $expr: {
      //               $and: [
      //                 { $eq: ["$route_code", "$$routCode"] },
      //                 { $eq: ["$distributor_code", "$$disCode"] },
      //               ],
      //             },
      //           },
      //         },
      //         {
      //           $project: {
      //             _id: 0,
      //             UNILEVER: 1,
      //           },
      //         },
      //       ],
      //       as: `shop`,
      //     },
      //   },
      // {
      //     $lookup: {
      //         from: 'census_shops',
      //         localField: `${tenant}.user`,
      //         foreignField: '_id',
      //         as: `${tenant}.user`,
      //       },
      // }
      // {
      //   $lookup: {
      //     from: "enumerators",
      //     let: { id: "$enumerator_id" },
      //     pipeline: [
      //       { $match: { $expr: { $eq: ["$_id", "$$id"] } } },
      //       { $project: { first_name: 1, last_name: 1, _id: 0 } },
      //     ],
      //     as: "enumerator",
      //   },
      // },
      // {
      //   $lookup: {
      //     from: "surveys",
      //     let: { id: "$survey_id" },
      //     pipeline: [
      //       { $match: { $expr: { $eq: ["$_id", "$$id"] } } },
      //       { $project: { name: 1, _id: 0 } },
      //     ],
      //     as: "survey_name",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$qc_user",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$enumerator",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$survey_name",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      //   {
      //     allowDiskUse: true,
      //   },
    ])
    .toArray();

  console.log({ completed, reponse });
  const result = [];
  for (const route of reponse) {
    let { distributor_code, route_code } = route;

    const reponse = await db
      .collection("census_shops")
      .find(
        {
          "UNILEVER.route_code": route_code,
          "UNILEVER.distributor_code": distributor_code,
        },
        { _id: 0, "UNILEVER.current_location.coordinates": 1 }
      )
      .toArray();
    if (reponse && reponse.length) {
      let { _id, UNILEVER, shop_details } = reponse[0];
      console.log("+_+_+_+_+_+", UNILEVER, shop_details);
      console.log(">>>>>>>>>", result);
      if (
        UNILEVER &&
        UNILEVER.current_location &&
        UNILEVER.current_location.coordinates
      ) {
        result.push({
          _id,
          shop_name: shop_details.name,
          coordinates: [
            UNILEVER.current_location.coordinates[1],
            UNILEVER.current_location.coordinates[0],
          ],
        });
      }
    }
  }
  fs.writeFileSync("./address.json", JSON.stringify(result));

  console.log("+++++++", result);

  console.log("Completed");
});
