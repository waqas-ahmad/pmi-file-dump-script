let tenant = {
  UNILEVER: 'UNILEVER',
  EBM: 'EBM',
  UPAISA: 'UPAISA',
  REDBULL: 'REDBULL',
  EBM_REGIONAL: 'EBM_REGIONAL',
  PCI: 'PCI',
  NFL: 'NFL',
  PCI_BOTTLER: 'PCI_BOTTLER',
  UL_RURAL: 'UL_RURAL',
  PMI: 'PMI',
  CENSUS: 'CENSUS',
  MLA_TEST: 'MLA_TEST',
};

let envs = {
  PRODUCTION: {
    database: '',
  },
  STAGING: {
    database: '-staging',
  },
};

let env = envs.PRODUCTION;

let database = {
  url: `mongodb+srv://census-admin-user:cHEKAuGBoDgWrRML@national-census.xbci9.mongodb.net/validator${env.database}?retryWrites=true&w=majority`,
  collection: { routes: 'routes', users: 'users' },
};

let database_census = {
  url: `mongodb+srv://census-admin-user:cHEKAuGBoDgWrRML@national-census.xbci9.mongodb.net/surveyauto?retryWrites=true&w=majority`,
  collection: { routes: 'routes', users: 'users' },
};

module.exports = {
  tenant,
  database,
  database_census,
};
