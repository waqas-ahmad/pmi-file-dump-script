const fs = require('fs');
const bcrypt = require('bcrypt');

let userDetails = {
  UNILEVER: {
    shortForm: 'UPL',
    password: '12345',
  },
  EBM: {
    shortForm: 'EBM',
    password: '12345',
  },
  UPAISA: {
    shortForm: 'UPAISA',
    password: '123456',
  },
  REDBULL: {
    shortForm: 'REDBULL',
    password: '12345',
  },
  EBM_REGIONAL: {
    shortForm: 'EBM_REGIONAL',
    password: '12345',
  },
  PCI: {
    skipSplit: true,
    shortForm: 'PCI',
    password: '12345',
  },
  NFL: {
    shortForm: 'NFL',
    password: '12345',
  },
  PCI_BOTTLER: {
    shortForm: 'PCI_BOTTLER',
    password: '12345',
  },
  UL_RURAL: {
    shortForm: 'UL_RURAL',
    password: '12345',
  },
  PMI: {
    shortForm: 'PMI',
    password: '12345',
  },
  CENSUS: {
    shortForm: 'CENSUS',
    password: '12345',
  },
};

function slugify(str) {
  str = str.replace(/^\s+|\s+$/g, '');
  str = str.toLowerCase();
  var from =
    'ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;';
  var to =
    'AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------';
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }
  str = str
    .replace(/[^a-z0-9 -]/g, '')
    .replace(/\s+/g, '_')
    .replace(/-+/g, '_');
  return str;
}

async function usersFromRoutes(routes, tenant) {
  console.log('Generating Users from Routes...');
  let users = [];
  for (let indexRoute = 0; indexRoute < routes.length; indexRoute++) {
    const route = routes[indexRoute];
    let usersEmails = route.route_code
      .split(',')
      .map(
        (u) =>
          `${slugify(userDetails[tenant]['skipSplit'] === true ? u : u.split('-')[0])}_${
            userDetails[tenant]['shortForm']
          }@gmail.com`
      );
    for (let indexUser = 0; indexUser < usersEmails.length; indexUser++) {
      const userEmail = usersEmails[indexUser];
      let prevUser = users.filter((user) => user.email === userEmail.toLowerCase());
      if (prevUser?.length) {
        prevUser[0].routes.push(route._id);
      } else {
        let user = {
          name: userEmail.split('@')[0].toUpperCase(),
          email: userEmail.toLowerCase(),
          password: await bcrypt.hash(userDetails[tenant].password, parseInt(10)),
          role: 'FIELD',
          tenant: tenant,
          routes: [route._id],
        };
        users.push(user);
      }
    }
  }
  // await fs.promises.writeFile(
  //   `./output-files/${tenant}_USERS.json`,
  //   JSON.stringify(users)
  // );
  return users;
}
module.exports = usersFromRoutes;
