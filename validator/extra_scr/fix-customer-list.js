const getDatabaseConnection = require("./../get-database-connection");
const { tenant, database } = require("./../config");
const polygonsToRoutes = require("./../polygons-to-routes");

const action = {
  REPLACE: 1,
  ADD: 2,
};

async function execute(
  tenant,
  query,
  routesDataFile,
  selectedAction,
  clientsOf
) {
  console.log("==================================================");
  console.log("Starting Adding Extra Clients...");

  console.log("==================================================");
  let dbConnection = await getDatabaseConnection(database);

  console.log("==================================================");
  let routes = await dbConnection
    .collection(database.collection.routes)
    .find({ tenant, ...query })
    .project({ _id: 1, distributor_code: 1, route_code: 1, all_route_codes: 1 })
    .toArray();

  for (let index = 0; index < routes.length; index++) {
    const routeData = routes[index];

    console.log(
      "Fixing",
      index,
      "/",
      routes.length,
      `${clientsOf}`,
      routeData["distributor_code"],
      routeData["route_code"]
    );

    let newCustomers = await polygonsToRoutes(
      [{ name: routeData[`${clientsOf}_code`] }],
      routesDataFile,
      tenant,
      "",
      `${clientsOf}CodeField`
    );

    await dbConnection.collection(database.collection.routes).updateOne(
      { _id: routeData._id },
      {
        ...(selectedAction === action.ADD
          ? {
              $addToSet: {
                customers: { $each: newCustomers[0]?.customers || [] },
              },
            }
          : {}),
        ...(selectedAction === action.REPLACE
          ? {
              $set: { customers: newCustomers[0]?.customers || [] },
            }
          : {}),
      }
    );
  }

  console.log();
  console.log("==================================================");
  console.log("Script Completed...");
  console.log("==================================================");
}

execute(
  tenant.UNILEVER,
  {
    distributor_code: {
      $in: [
        "15510451",
        "15551847",
        "15492823",
        "50250227",
        "15551848",
        "15167985",
        "50250213",
        "15549711",
        "15383095",
        "15581289"
     ],
    },
  },
  "/home/ubuntu/Downloads/07_Distribution/10 Distributors_Customer_Listing.JSON",
  action.REPLACE,
  "distributor"
);
