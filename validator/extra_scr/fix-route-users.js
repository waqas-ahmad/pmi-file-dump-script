const getDatabaseConnection = require("./../get-database-connection");
const { tenant, database } = require("./../config");
const usersFromRoutes = require("./../users-from-routes");

let rolePostString = {
  UNILEVER: {
    field: "",
    supervisor: "_SUPR",
    admin: "",
  },
  EBM: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  UPAISA: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  REDBULL: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  EBM_REGIONAL: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  PCI: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  NFL: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  PCI_BOTTLER: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  UL_RURAL: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  PMI: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
  CENSUS: {
    field: "",
    supervisor: "_SUPERVISOR",
    admin: "",
  },
};

async function execute(tenant, query, roles) {
  console.log("==================================================");
  console.log("Starting Fixing Route Users...");

  console.log("==================================================");
  let dbConnection = await getDatabaseConnection(database);

  console.log("==================================================");

  if (roles.includes("FIELD")) {
    let routes = await dbConnection
      .collection(database.collection.routes)
      .find({ tenant, ...query })
      .project({ _id: 1, route_code: 1, all_route_codes: 1 })
      .toArray();

    for (let index = 0; index < routes.length; index++) {
      const routeData = routes[index];

      console.log("Fixing", routeData["route_code"]);

      let routeCodes = [
        ...new Set([
          routeData["route_code"],
          ...(routeData.all_route_codes || []),
        ]),
      ];

      console.log(routeCodes);

      if (routeCodes.length) {
        for (
          let indexExtraRoutes = 0;
          indexExtraRoutes < routeCodes.length;
          indexExtraRoutes++
        ) {
          const route = routeCodes[indexExtraRoutes];

          console.log("Processing", route);

          let newUsers = await usersFromRoutes(
            [
              {
                _id: routeData._id,
                route_code: `${route}${rolePostString[tenant].field}`,
              },
            ],
            tenant
          );
          for (let _index = 0; _index < newUsers.length; _index++) {
            const newUser = newUsers[_index];
            let users = await dbConnection
              .collection(database.collection.users)
              .find({ email: newUser.email })
              .project({ _id: 1 })
              .toArray();

            if (users[0]?._id) {
              console.log("Updating User", newUser.email, "...");
              await dbConnection
                .collection(database.collection.users)
                .updateOne(
                  { _id: users[0]._id },
                  {
                    $addToSet: { routes: { $each: newUser?.routes || [] } },
                  }
                );
            } else {
              console.log("Adding User", newUser.email, "...");
              await dbConnection
                .collection(database.collection.users)
                .insertOne(newUser);
            }
          }
        }
      }
      console.log("==================================================");
    }
  }
  if (roles.includes("SUPERVISOR") || roles.includes("ADMIN")) {
    let distributors = await dbConnection
      .collection(database.collection.routes)
      .aggregate([
        { $match: { tenant, ...query } },
        { $group: { _id: "$distributor_code", routes: { $push: "$_id" } } },
        {
          $project: {
            _id: 0,
            distributor_code: "$_id",
            route_codes: "$routes",
          },
        },
      ])
      .toArray();

    for (let index = 0; index < distributors.length; index++) {
      const distributorData = distributors[index];

      console.log("Fixing", distributorData["distributor_code"]);

      let distributor = distributorData["distributor_code"];

      console.log("Processing", distributor);

      newUsers = [
        ...(roles.includes("ADMIN")
          ? (
              await usersFromRoutes(
                [
                  {
                    _id: distributorData.route_codes[0],
                    route_code: `${distributor}${rolePostString[tenant].admin}`,
                  },
                ],
                tenant
              )
            ).map((user) => ({
              ...user,
              role: "ADMIN",
              routes: distributorData.route_codes,
            }))
          : []),
        ...(roles.includes("SUPERVISOR")
          ? (
              await usersFromRoutes(
                [
                  {
                    _id: distributorData.route_codes[0],
                    route_code: `${distributor}${rolePostString[tenant].supervisor}`,
                  },
                ],
                tenant
              )
            ).map((user) => ({
              ...user,
              role: "SUPERVISOR",
              routes: distributorData.route_codes,
            }))
          : []),
      ];
      for (let _index = 0; _index < newUsers.length; _index++) {
        const newUser = newUsers[_index];
        let users = await dbConnection
          .collection(database.collection.users)
          .find({ email: newUser.email })
          .project({ _id: 1 })
          .toArray();

        if (users[0]?._id) {
          console.log("Updating User", newUser.email, "...");
          await dbConnection.collection(database.collection.users).updateOne(
            { _id: users[0]._id },
            {
              $addToSet: { routes: { $each: newUser?.routes || [] } },
            }
          );
        } else {
          console.log("Adding User", newUser.email, "...");
          await dbConnection
            .collection(database.collection.users)
            .insertOne(newUser);
        }
      }
      console.log("==================================================");
    }
  }
  if (roles.includes("GEO")) {
    let distributors = await dbConnection
      .collection(database.collection.routes)
      .aggregate([
        { $match: { tenant, ...query } },
        { $group: { _id: "$geo", routes: { $push: "$_id" } } },
        {
          $project: {
            _id: 0,
            distributor_code: "$_id",
            route_codes: "$routes",
          },
        },
      ])
      .toArray();

    for (let index = 0; index < distributors.length; index++) {
      const distributorData = distributors[index];

      console.log("Fixing", distributorData["distributor_code"]);

      let distributor = distributorData["distributor_code"];

      console.log("Processing", distributor);

      newUsers = (
        await usersFromRoutes(
          [
            {
              _id: distributorData.route_codes[0],
              route_code: `${distributor}${rolePostString[tenant].admin}`,
            },
          ],
          tenant
        )
      ).map((user) => ({
        ...user,
        role: "ADMIN",
        routes: distributorData.route_codes,
      }));
      for (let _index = 0; _index < newUsers.length; _index++) {
        const newUser = newUsers[_index];
        let users = await dbConnection
          .collection(database.collection.users)
          .find({ email: newUser.email })
          .project({ _id: 1 })
          .toArray();

        if (users[0]?._id) {
          console.log("Updating User", newUser.email, "...");
          await dbConnection.collection(database.collection.users).updateOne(
            { _id: users[0]._id },
            {
              $addToSet: { routes: { $each: newUser?.routes || [] } },
            }
          );
        } else {
          console.log("Adding User", newUser.email, "...");
          await dbConnection
            .collection(database.collection.users)
            .insertOne(newUser);
        }
      }
      console.log("==================================================");
    }
  }
  if (roles.includes("SUPER_ADMIN")) {
    let distributors = await dbConnection
      .collection(database.collection.routes)
      .aggregate([
        { $match: { tenant } },
        { $group: { _id: "ADMIN", routes: { $push: "$_id" } } },
        {
          $project: {
            _id: 0,
            distributor_code: "$_id",
            route_codes: "$routes",
          },
        },
      ])
      .toArray();

    for (let index = 0; index < distributors.length; index++) {
      const distributorData = distributors[index];

      console.log("Fixing", distributorData["distributor_code"]);

      let distributor = distributorData["distributor_code"];

      console.log("Processing", distributor);

      newUsers = (
        await usersFromRoutes(
          [
            {
              _id: distributorData.route_codes[0],
              route_code: `${distributor}${rolePostString[tenant].admin}`,
            },
          ],
          tenant
        )
      ).map((user) => ({
        ...user,
        role: "ADMIN",
        routes: distributorData.route_codes,
      }));

      for (let _index = 0; _index < newUsers.length; _index++) {
        const newUser = newUsers[_index];
        let users = await dbConnection
          .collection(database.collection.users)
          .find({ email: newUser.email })
          .project({ _id: 1 })
          .toArray();

        if (users[0]?._id) {
          console.log("Updating User", newUser.email, "...");
          await dbConnection.collection(database.collection.users).updateOne(
            { _id: users[0]._id },
            {
              $set: { routes: newUser?.routes || [] },
            }
            // {
            //   $addToSet: { routes: { $each: newUser?.routes || [] } },
            // }
          );
        } else {
          console.log("Adding User", newUser.email, "...");
          await dbConnection
            .collection(database.collection.users)
            .insertOne(newUser);
        }
      }
      console.log("==================================================");
    }
  }

  console.log();
  console.log("==================================================");
  console.log("Script Completed...");
  console.log("==================================================");
}

execute(tenant.UNILEVER, { distributor_code: "15581289" }, [
  "SUPER_ADMIN",
  "ADMIN",
  "SUPERVISOR",
  // 'GEO',
  // 'FIELD',
]);
