const getDatabaseConnection = require("./../get-database-connection");
const { tenant, database } = require("./../config");
const kmlToJson = require("./../kml-to-json");
const { default: mongoose } = require("mongoose");

async function execute(tenant, query, kmlFiles) {
  console.log("==================================================");
  console.log("Starting Fixing Routes Boundaries...");

  console.log("==================================================");
  let dbConnection = await getDatabaseConnection(database);

  console.log("==================================================");
  let polygons = await kmlToJson(kmlFiles, tenant);

  console.log("==================================================");
  console.log("Getting Routes Data");

  let routes = await dbConnection
    .collection(database.collection.routes)
    .find({ tenant, ...query })
    .project({ _id: 1, route_code: 1 })
    .toArray();

  console.log(routes.length, "Routes found");
  console.log("==================================================");

  for (let index = 0; index < routes.length; index++) {
    const routeData = routes[index];

    console.log();
    console.log("Fixing", routeData["route_code"]);

    let routeNewData = polygons.filter(
      (data) => data.name === routeData["route_code"]
    )[0];

    if (routeNewData) {
      console.log("Updating Boundary");
      await dbConnection.collection(database.collection.routes).updateOne(
        { _id: new mongoose.Types.ObjectId(routeData._id) },
        {
          $set: {
            boundary: {
              coordinates: routeNewData.boundary?.coordinates,
              type: "Polygon",
              center: routeNewData.center?.coordinates,
            },
          },
        }
      );
    } else {
      console.log("No Data in KML File");
    }
  }

  console.log();
  console.log("==================================================");
  console.log("Script Completed...");
  console.log("==================================================");
}

execute(
  tenant.UNILEVER,
  {
    route_code: {
      $in: ["ZENR01,ZENR02"],
    },
  },
  ["/home/ubuntu/Downloads/ZAIN ENTERPRISES-50250118 FSD.kml"]
);
