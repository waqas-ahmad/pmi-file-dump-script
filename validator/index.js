const kmlToJson = require("./kml-to-json");
const sanitizeOverlappingPolygons = require("./sanitize-overlapping-polygons");
const polygonsToRoutes = require("./polygons-to-routes");
const dumpRoutes = require("./dump-routes");
const usersFromRoutes = require("./users-from-routes");
const dumpUsers = require("./dump-users");
const getDatabaseConnection = require("./get-database-connection");

const { tenant, database } = require("./config");

async function execute(kmlFiles, tenant, geo, routesDataFile) {
  console.log("==================================================");
  console.log("Starting Script...");

  console.log("==================================================");
  let dbConnection = await getDatabaseConnection(database);

  console.log("==================================================");
  let polygons = await kmlToJson(kmlFiles, tenant);
  
  console.log("==================================================");
  polygons = await sanitizeOverlappingPolygons(polygons, tenant);

  console.log("==================================================");
  let routes = await polygonsToRoutes(polygons, routesDataFile, tenant, geo);

  console.log("==================================================");
  routes = await dumpRoutes(dbConnection, routes, database, tenant);

  console.log("==================================================");
  let users = await usersFromRoutes(routes, tenant);

  console.log("==================================================");
  users = await dumpUsers(dbConnection, users, database, tenant);

  console.log("==================================================");
  console.log("Script Completed...");
  console.log("==================================================");
}

execute(
  ["/home/ubuntu/Downloads/ABDUL SAMI TRADERS-15581289 HYD (1).kml"],
  tenant.UNILEVER,
  "HYDERABAD",
  "/home/ubuntu/Downloads/07_Distribution/10 Distributors_Customer_Listing.json"
);

// [
//   "FAISALABAD"
//   "RAHIM YAR KHAN"
//   "LAHORE",
//   "MULTAN",
//   "HYDERABAD", 
//   "ISLAMABAD"
// ]
