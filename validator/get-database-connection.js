const mongoose = require("mongoose");

async function getDatabaseConnection(database) {
  console.log("Connecting to Database...");

  mongoose.connect(database.url);

  let db = mongoose.connection;

  mongoose.set("debug", false);

  await new Promise((resolve, reject) => {
    db.on("error", () => reject());
    db.once("open", async () => {
      console.log("Database Connected");
      resolve();
    });
  });

  return db;

}
module.exports = getDatabaseConnection;