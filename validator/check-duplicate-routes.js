const fs = require('fs');
const mongoose = require('mongoose');

async function checkDuplicateRoutes(dbConnection, routes, database, tenant) {
  console.log('Checking Duplicate Routes from Database...');

  let valueRouteKey = routes.map((route) => {
    return `${route.route_code}---${route.distributor_code}`;
  });
  let isDuplicate = valueRouteKey.some((routeKey, idx) => {
    let duplicateIndex = valueRouteKey.indexOf(routeKey);
    if (duplicateIndex != idx) {
      console.log(valueRouteKey[duplicateIndex]);
      return true;
    }
    return false;
  });
  if (isDuplicate) {
    console.log('Duplicate Found in Keys', isDuplicate);
    return;
  }

  for (let index = 0; index < routes.length; index++) {
    const routeItem = routes[index];
    let result = await dbConnection.collection(database.collection.routes).count({
      tenant,
      route_code: routeItem.route_code,
      distributor_code: routeItem.distributor_code,
    });
    if (result > 0) {
      console.log('Duplicate Found For', routeItem.distributor_code, routeItem.route_code);
    }
  }
  return routes;
}
module.exports = checkDuplicateRoutes;
