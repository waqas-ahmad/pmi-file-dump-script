const fs = require('fs');

let filesData = {
  // UNILEVER: {
  //   sheetName: "Customer Data",
  //   territoryField: "Territory",
  //   distributorCodeField: "Distributor Code",
  //   distributorNameField: "DT name",
  //   routeCodeField: "Route Code",
  //   routeNameField: "Route Name",
  //   customerCodeField: "Customer Code",
  //   customerNameField: "Customer Name",
  //   distributorSameAsRoute: false,
  //   extraFields: {},
  // },
  // UNILEVER: {
  //   sheetName: 'Sheet1',
  //   territoryField: 'Region',
  //   distributorCodeField: 'Distributor Code',
  //   distributorNameField: 'Distributor Name',
  //   routeCodeField: 'Route Code',
  //   routeNameField: 'Route Name',
  //   customerCodeField: 'Customer Code',
  //   customerNameField: 'Customer Name',
  //   distributorSameAsRoute: false,
  //   extraFields: {},
  // },
  // UNILEVER: {
  //   sheetName: 'Sheet1',
  //   territoryField: 'Region',
  //   distributorCodeField: 'Distributor Code',
  //   distributorNameField: 'DT name',
  //   routeCodeField: 'Route Code',
  //   routeNameField: 'Route Name',
  //   customerCodeField: 'Customer Code',
  //   customerNameField: 'Customer Name',
  //   distributorSameAsRoute: false,
  //   extraFields: {},
  // },
  // UNILEVER: {
  //   sheetName: 'Sheet1',
  //   territoryField: 'Region',
  //   distributorCodeField: 'DT Code',
  //   distributorNameField: 'DT Name',
  //   routeCodeField: 'Route Code',
  //   routeNameField: 'Route Name',
  //   customerCodeField: 'Customer Code',
  //   customerNameField: 'Customer Name',
  //   distributorSameAsRoute: false,
  //   extraFields: {},
  // },
  UNILEVER: {
    sheetName: 'Sheet1',
    territoryField: 'Region',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name',
    routeCodeField: 'Route Code',
    routeNameField: 'Route Name',
    customerCodeField: 'Customer Code',
    customerNameField: 'Customer Name',
    distributorSameAsRoute: false,
    extraFields: {},
  },
  UL_RURAL: {
    sheetName: 'Sheet1',
    territoryField: '',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name',
    routeCodeField: 'route',
    routeNameField: 'Route Name',
    customerCodeField: 'tempKey',
    customerNameField: 'Customer Name',
    customerLatField: 'lat',
    customerLngField: 'lng',
    distributorSameAsRoute: false,
    requireCustomerCoordinates: true,
    skipSplit: true,
    within_boundary: true,
    extraFields: {},
  },
  EBM: {
    sheetName: 'Sheet1',
    territoryField: 'Territory Name',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name ',
    routeCodeField: 'Order Booker Code ',
    routeNameField: 'Order Booker Name',
    customerCodeField: 'Customer code',
    customerNameField: 'Customer name',
    distributorSameAsRoute: false,
    extraFields: {},
  },
  UPAISA: {
    sheetName: 'data',
    territoryField: '',
    distributorCodeField: '',
    distributorNameField: '',
    routeCodeField: '',
    routeNameField: '',
    customerCodeField: '',
    customerNameField: '',
    distributorSameAsRoute: true,
    extraFields: { restrict_population: true, shop_id: [] },
  },
  REDBULL: {
    sheetName: 'Sheet1',
    territoryField: '',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name',
    routeCodeField: 'OrderBooker Code',
    routeNameField: 'OrderBooker Name',
    customerCodeField: 'Store Code',
    customerNameField: 'Store Name',
    customerLatField: 'Lat',
    customerLngField: 'Long',
    distributorSameAsRoute: false,
    requireCustomerCoordinates: true,
    extraFields: {},
  },
  EBM_REGIONAL: {
    sheetName: 'Sheet1',
    territoryField: 'Region',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name',
    routeCodeField: 'Route Code',
    routeNameField: 'Route Name',
    customerCodeField: 'Customer Code',
    customerNameField: 'Customer Name',
    distributorSameAsRoute: false,
    extraFields: {},
  },
  NFL: {
    sheetName: 'Sheet1',
    territoryField: 'Region',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name',
    routeCodeField: 'Order Booker Code',
    routeNameField: 'Order Booker Name',
    customerCodeField: 'Store Code',
    customerNameField: 'Store Name',
    distributorSameAsRoute: false,
    extraFields: {},
  },
  PMI: {
    sheetName: 'Sheet1',
    territoryField: 'Distributor City',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name',
    routeCodeField: 'route_code',
    routeNameField: 'area',
    customerCodeField: 'customer_code',
    customerNameField: 'customer_code',
    customerLatField: 'lat_census',
    customerLngField: 'lng_census',
    distributorSameAsRoute: false,
    requireCustomerCoordinates: true,
    within_boundary: true,
    extraFields: {},
  },
  PCI: {
    sheetName: 'Sheet1',
    territoryField: 'Distributor City',
    distributorCodeField: 'Distributor Code',
    distributorNameField: 'Distributor Name',
    routeCodeField: 'Route Code',
    routeNameField: 'Route Description',
    customerCodeField: 'Customer Code',
    customerNameField: 'Customer Name',
    customerLatField: 'Latitude',
    customerLngField: 'Longitude',
    distributorSameAsRoute: false,
    requireCustomerCoordinates: true,
    skipSplit: true,
    within_boundary: false,
    extraFields: {},
  },
};

async function polygonsToRoutes(polygons, dataFile, tenant, geo, matchField = 'routeCodeField') {
  console.log('Getting Routes From Polygons...');
  const customers = (dataFile && JSON.parse(JSON.stringify(require(dataFile)))) || {
    [filesData[tenant].sheetName]: [],
  };
  let routes = polygons.map((route) => {
    let updatedRoute = customers[filesData[tenant].sheetName].filter((rt) =>
      route.name
        .split(',')
        .map((code) => (filesData[tenant].skipSplit === true ? code : code.split('-')[0].trim()))
        .includes(rt[filesData[tenant][matchField]])
    );
    let routeData = route.name;
    let routeCodes = (
      filesData[tenant].skipSplit === true ? routeData : routeData.split('-')[0]
    ).split(',');
    let name = [];
    for (const routeCode of routeCodes) {
      console.log(routeCode);
      name.push(
        customers[filesData[tenant].sheetName].filter(
          (data) => data[filesData[tenant].routeCodeField] === routeCode
        )[0]?.[filesData[tenant].routeNameField]
      );
    }
    let newData = {
      ...route,
      boundary: {
        coordinates: route.boundary?.coordinates,
        type: 'Polygon',
        center: route.center?.coordinates,
      },
      territory: updatedRoute?.[0]?.[filesData[tenant].territoryField] || geo,
      distributor_code:
        filesData[tenant].distributorSameAsRoute === true
          ? route.name
          : updatedRoute?.[0]?.[filesData[tenant].distributorCodeField] || '',
      distributor_name:
        filesData[tenant].distributorSameAsRoute === true
          ? route.name
          : updatedRoute?.[0]?.[filesData[tenant].distributorNameField] || '',
      route_code:
        route.name ||
        updatedRoute?.[0]?.[filesData[tenant].routeCodeField] ||
        filesData[tenant].distributorSameAsRoute === true
          ? route.name
          : '',
      route_name: filesData[tenant].distributorSameAsRoute === true ? route.name : name.join(', '),
      customers: updatedRoute.map((cm) => ({
        customer_id: cm[filesData[tenant].customerCodeField],
        customer_name: cm[filesData[tenant].customerNameField],
        ...(filesData[tenant].requireCustomerCoordinates &&
        cm[filesData[tenant].customerLngField] &&
        cm[filesData[tenant].customerLatField]
          ? {
              customer_location: {
                type: 'Point',
                coordinates: [
                  cm[filesData[tenant].customerLngField].trim(),
                  cm[filesData[tenant].customerLatField].trim(),
                ],
              },
              ...(filesData[tenant].within_boundary === true ? { within_boundary: true } : {}),
            }
          : {}),
      })),
      tenant: tenant,
      geo: geo,
      ...filesData[tenant].extraFields,
    };
    delete newData.folder;
    delete newData.center;
    delete newData.name;
    return newData;
  });
  // await fs.promises.writeFile(
  //   `./output-files/${tenant}_ROUTES.json`,
  //   JSON.stringify(routes)
  // );
  return routes;
}
module.exports = polygonsToRoutes;
