const fs = require("fs");

async function dumpUsers(dbConnection, users, database, tenant) {
  console.log("Dumping Users to Database...");

  for (let index = 0; index < users.length; index++) {
    const userItem = users[index];

    let usersFound = await dbConnection
      .collection(database.collection.users)
      .find({ email: userItem.email })
      .project({ _id: 1 })
      .toArray();

    if (usersFound[0]?._id) {
      await dbConnection.collection(database.collection.users).updateOne(
        { _id: usersFound[0]._id },
        {
          $addToSet: { routes: { $each: userItem?.routes || [] } },
        }
      );
      users[index]["_id"] = users[0]?._id;
    } else {
      let result = await dbConnection
        .collection(database.collection.users)
        .insertOne(userItem);
      users[index]["_id"] = result.insertedId;
    }
  }
  // await fs.promises.writeFile(
  //   `./output-files/${tenant}_USERS_DUMPED.json`,
  //   JSON.stringify(users)
  // );
  return users;
}
module.exports = dumpUsers;
