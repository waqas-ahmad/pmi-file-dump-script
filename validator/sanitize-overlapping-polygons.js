const fs = require("fs");
let turf = require("@turf/turf");

function getFeature(coordinates) {
  return {
    type: "Feature",
    geometry: {
      type: "Polygon",
      coordinates: coordinates,
    },
  };
}

async function sanitizeOverlapingPolygons(polygons, polygonFile) {
  console.log("Removing overlaping polygons...", polygons);

  for (let i = 0; i < polygons.length; i++) {
    for (let j = i + 1; j < polygons.length; j++) {
      // polygons[j].boundary.coordinates =
        // turf.difference(
        //   getFeature(polygons[j].boundary.coordinates),
        //   getFeature(polygons[i].boundary.coordinates)
        // ).geometry.coordinates;
    }
  }
  // await fs.promises.writeFile(
  //   `./output-files/${polygonFile}_POLYGONS_SENITIZED.json`,
  //   JSON.stringify(polygons)
  // );
  return polygons;
}
module.exports = sanitizeOverlapingPolygons;
