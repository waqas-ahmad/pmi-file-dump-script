const fs = require("fs");
const xml2js = require("xml2js");
const parser = new xml2js.Parser({ attrkey: "ATTR" });

var center = function (arr) {
  var x = arr.map(([lng]) => lng);
  var y = arr.map(([_, lat]) => lat);
  var lng = (Math.min(...x) + Math.max(...x)) / 2;
  var lat = (Math.min(...y) + Math.max(...y)) / 2;
  return { type: "Point", coordinates: [lng, lat] };
};

async function kmlToJson(kmlFiles, polygonFile, removeSpaces = true) {
  console.log("Generating Polygons from kml...");
  let allPolygons = [];
  for (let index = 0; index < kmlFiles.length; index++) {
    const kmlFilePath = kmlFiles[index];
    let kmlFileData = await fs.promises.readFile(`${kmlFilePath}`, "utf-8");
    const result = await new Promise((resolve, reject) =>
      parser.parseString(kmlFileData, async function (error, data) {
        if (error) reject(error);
        else resolve(data);
      })
    );

    let polygons = [];
    let folderData = result.kml.Document[0];
    // for (let folderData of kmlData) {
    let Placemark = folderData.Placemark;
    if (!Placemark) continue;
    for (let coo of Placemark) {
      if (!coo.Polygon) continue;
      let name = coo.name[0];
      let coordinates =
        coo.Polygon[0].outerBoundaryIs[0].LinearRing[0].coordinates;
      let split = coordinates[0].split(" ");
      let coord = [];
      for (let e of split) {
        let eSplit = e.split(",");
        if (eSplit.length === 1) continue;
        if (!isNaN(parseFloat(eSplit[1])) || !isNaN(parseFloat(eSplit[0])))
          coord.push([parseFloat(eSplit[0]), parseFloat(eSplit[1])]);
      }
      polygons.push({
        boundary: {
          coordinates: [coord],
          type: "Polygon",
        },
        center: center(coord),
        name: removeSpaces ? name.split(" ")[0] : name.trim(),
        folder: folderData.name[0],
      });
    }
    // }
    console.log(polygons.length, "Polygons in ", kmlFilePath.split("/").pop());
    allPolygons = [...allPolygons, ...polygons];
    if (index === kmlFiles.length - 1) {
      console.log("Total Polygons: ", allPolygons.length);
      // await fs.promises.writeFile(
      //   `./output-files/${polygonFile}_POLYGONS.json`,
      //   JSON.stringify(allPolygons)
      // );
      // console.log("Polygon File Saved...");
      return allPolygons;
    }
  }
}
module.exports = kmlToJson;
