const fs = require("fs");
const mongoose = require("mongoose");

async function dumpRoutes(dbConnection, routes, database, tenant) {
  console.log("Dumping Routes to Database...");

  for (let index = 0; index < routes.length; index++) {
    const routeItem = routes[index];
    let result = await dbConnection
      .collection(database.collection.routes)
      .insertOne(routeItem);
    routes[index]['_id'] = result.insertedId;
  }
  // await fs.promises.writeFile(
  //   `./output-files/${tenant}_ROUTES_DUMPED.json`,
  //   JSON.stringify(routes)
  // );
  return routes;
}
module.exports = dumpRoutes;