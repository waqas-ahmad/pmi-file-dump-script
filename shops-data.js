const csv = require("csvtojson");
const fs = require("fs");
const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://pmi-admin-user:CRDbFpQDzzP2sSlu@pmi-cluster.cnhta.mongodb.net/pmi-development?retryWrites=true&w=majority"
);
let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async () => {
  console.log("Connection Established");
  csv()
    .fromFile("./sales_pivot_table_info_added_restructured_with_tamid.csv")
    .then(async (json) => {
      let completed = -1;
      let temp_Array = [];
      console.log({ jsonLength: json.length });
      for (const row of json) {
        completed++;
        if (completed <= 220000) continue;
        if (temp_Array.length === 5000) {
          await db.collection("shops_data_new").insertMany([...temp_Array]);
          temp_Array = [];
          console.log({ completed });
        }
        let temp_row = {};
        for (const colName in row) {
          let colValue = row[colName];
          if (colName.includes("Sales")) {
            colValue = parseFloat(colValue);
          }
          if (["Longitude", "Latitude"].includes(colName)) {
            let lng = parseFloat(row["Longitude"]) || 0.0;
            let lat = parseFloat(row["Latitude"]) || 0.0;
            if (lng > 180 || lat > 90 || lng < -180 || lat < -90) {
              console.log({ lat, lng });
              lng = 0;

              lat = 0;
            }
            temp_row["location"] = {
              type: "Point",
              coordinates: [lng, lat],
            };
            delete row.longitude;
            delete row.latitude;
          } else {
            temp_row[colName.replace(/ /g, "_").toLocaleLowerCase()] = colValue;
          }
        }
        temp_Array.push({ ...temp_row });
      }
      await db.collection("shops_data_new").insertMany([...temp_Array]);
      temp_Array = [];
    });
});
