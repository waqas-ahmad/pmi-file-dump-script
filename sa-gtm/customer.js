const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://stage-npt-admin-user:U9ZfUVDQX8NRoRHs@npt-stage-cluster.mkspv.mongodb.net/stage-npt-jazz?retryWrites=true&w=majority"
);
let db = mongoose.connection;
const routes = require("./dammam.json").routes;

db.once("open", async () => {
  console.log("connected");

  for (const elem of routes) {
    const { route_code, customers } = elem;
    let query = { route_code: route_code };
    const route = await db.collection("routes").findOne(query);
    console.log(route);
    const { _id } = route;
    if (_id) {
      for (const Customer of customers) {
        const { customer_id, customer_name, sec, type, coordinates } = Customer;
        let query = { customer_id: customer_id };
        let update = {
          $set: {
            shop_name: customer_name,
            customer_id: customer_id,
            route_id: _id,
            sec: sec,
            type: type,
            coordinates: coordinates,
          },
        };
        let options = { new: true, upsert: true };
        await db
          .collection("customers")
          .findOneAndUpdate(query, update, options);
      }
      console.log("Complete...", route_code);
    } else {
      console.log("route id not found against", route_code);
    }
  }
  console.log("customers Dump successfully ");
});
