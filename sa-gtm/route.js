const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://stage-npt-admin-user:U9ZfUVDQX8NRoRHs@npt-stage-cluster.mkspv.mongodb.net/stage-npt-jazz?retryWrites=true&w=majority"
);
const { v4: uuidv4 } = require("uuid");

let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));

const data = require("./riyadh.json");
const { tenant, country, city, routes } = data;

db.once("open", async () => {
  console.log("connected");
  const response = await db.collection("tenant").findOne({ name: tenant });
  // if (response === null) {
  //   const res = await db.collection("tenant").insertOne({
  //     name: tenant,
  //     country: [{ name: country, city: [{ name: city, id: uuidv4() }] }],
  //   });
  // }
  const { _id: tenant_id } = response;
  const { id: city_id } = response.country
    .find((elem) => elem.name === country)
    .city.find((elem) => elem.name === city);
  if (city_id && tenant_id) {
    for (const elem of routes) {
      //   console.log(">>>>>>", elem);

      const { route_code, sales_rep, id } = elem;

      let query = { route_code: route_code, city_id: city_id };
      console.log(route_code);
      let update = {
        $set: {
          route_code: route_code,
          id: id,
          tenant_id: tenant_id,
          city_id: city_id,
          sales_rep: sales_rep,
        },
      };
      let options = { new: true, upsert: true };
      let model = await db
        .collection("routes")
        .findOneAndUpdate(query, update, options);

      // for (const Customer of customers) {
      //   const { customer_id, customer_name, sec, type, coordinates } = Customer;
      // }
    }
  } else {
    console.log("City id not found", city_id);
  }

  console.log("routes successfully dump");
});

console.log(tenant);
