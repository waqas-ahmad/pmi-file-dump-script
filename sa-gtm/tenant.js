const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://stage-npt-admin-user:U9ZfUVDQX8NRoRHs@npt-stage-cluster.mkspv.mongodb.net/stage-npt-jazz?retryWrites=true&w=majority"
);
const { v4: uuidv4 } = require("uuid");

let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));

const data = require("./dammam.json");
const { tenant, country, city, routes } = data;

db.once("open", async () => {
  console.log("connected");
  let query = {
    name: tenant,
    "country.name": country,
  };
  let update = {
    $push: {
      "country.$[country].city": { name: city, id: uuidv4() },
    },
  };
  let model = await db.collection("tenant").updateOne(query, update, {
    arrayFilters: [{ "country.name": "KSA" }],
    upsert: true,
  });
  console.log(model);
  console.log("Script complete");
});
