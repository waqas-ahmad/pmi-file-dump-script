const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://harpic-dtd-prod-user:GoBJAbjRjZK1dyRq@cluster-dh0q6m4g.gpdqz.mongodb.net/harpic_dtd_prod?retryWrites=true&w=majority"
);
let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async () => {
  let completed = 0;
  let temp_Array = [];
  // let teams = await db
  //   .collection("teams")
  //   .find({})
  //   .project({ _id: 0, anchor_id: 1, inviter_ids: 1 })
  //   .toArray();
  // const anchorIds = teams.map((elem) => elem.anchor_id);

  // console.log(anchorIds);

  // const anchorSubmissions = await db
  //   .collection("anchor_submission_copy")
  //   .find({ user_id: { $in: anchorIds } })
  //   .project({
  //     _id: 0,
  //     activity_ended_at: 1,
  //     activity_started_at: 1,
  //     qrCode: 1,
  //     program_id: 1,
  //   })
  //   .toArray();

  const anchorSubmissions = await db
    .collection("anchor_submission_copy")
    .aggregate([
      // Stage 1: Filter pizza order documents by pizza size
      {
        $group: {
          _id: "$user_id",
          data: {
            $push: {
              activity_ended_at: "$activity_ended_at",
              activity_started_at: "$activity_started_at",
              qrCodes: { $split: ["$qrCode", ","] },
              program_id: "$program_id",
              activity_date: {
                $dateToString: {
                  format: "%Y-%m-%d",
                  date: "$activity_started_at",
                },
              },
            },
          },
        },
      },
      {
        $lookup: {
          from: "teams",
          localField: "_id",
          foreignField: "anchor_id",
          as: "Team",
        },
      },
      {
        $unwind: "$Team",
      },
      {
        $sort: { activity_started_at: -1 },
      },
    ])
    .toArray();

  console.log(anchorSubmissions);
  for (const elem of anchorSubmissions) {
    for (let i = 0; i < elem.data.length - 1; i++) {
      // const inviterIds = await db
      //   .collection("inviter_submission_copy")
      //   .find({ qr_code: { $in: elem.data[i].qrCodes } })
      //   .project({ _id: 1, qr_code: 1 })
      //   .toArray();
      // console.log(
      //   elem.data[i].qrCodes.filter(
      //     (qr) => !inviterIds.map((id) => id.qr_code).includes(qr)
      //   )
      // );
      // console.log(
      //   elem.data[i].qrCodes.map((qr) => ({
      //     qr,
      //     found: inviterIds.filter(
      //       (inviterSubmission) => inviterSubmission.qr_code === qr
      //     ),
      //   }))
      // );
      if (i === elem.data.length - 1) {
        let { activity_started_at } = elem.data[i];

        const inviterSubmission = await db
          .collection("inviter_submission_copy")
          .find({
            ended_at: activity_started_at,
            enumerator_id: { $in: elem.Team.inviter_ids },
          })
          .project({ _id: 1, qr_code: 1 })
          .toArray();
        const inviterIds = await db
          .collection("inviter_submission_copy")
          .find({ qr_code: { $in: elem.data[i].qrCodes } })
          .project({ _id: 1, qr_code: 1 })
          .toArray();
        console.log(">>>>.", inviterSubmission);
      } else {
        let { activity_started_at } = elem.data[i];
        let { activity_ended_at } = elem.data[i + 1];
        const inviterSubmission = await db
          .collection("inviter_submission_copy")
          .find({
            $and: [
              { ended_at: { $lt: activity_started_at } },
              { conducted_at: { $gt: activity_ended_at } },
              { enumerator_id: { $in: elem.Team.inviter_ids } },
            ],
          })
          .project({ _id: 1, qr_code: 1 })
          .toArray();
        const inviterIds = await db
          .collection("inviter_submission_copy")
          .find({ qr_code: { $in: elem.data[i].qrCodes } })
          .project({ _id: 1, qr_code: 1 })
          .toArray();
        console.log(inviterSubmission);
      }
    }
  }
  console.log({ completed });

  console.log("Completed");
});
