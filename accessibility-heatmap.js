const lahore = require("./data/lahore/accessibility_heatmap.json");
const islamabad = require("./data/islamabad/accessibility_heatmap.json");
const karachi = require("./data/karachi/accessibility_heatmap.json");
const ULA_ALL = {
  lahore: [...lahore],
  islamabad: [...islamabad],
  karachi: [...karachi],
};

const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://pmi-admin-user:CRDbFpQDzzP2sSlu@pmi-cluster.cnhta.mongodb.net/pmi-development?retryWrites=true&w=majority"
);
let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async () => {
  let completed = 0;
  let temp_Array = [];
  for (const city in ULA_ALL) {
    const selected_city_locations = ULA_ALL[city];
    for (const location of selected_city_locations) {
      let temp_row = {};
      temp_row.city = city;
      temp_row.value = location.value;
      temp_row.population = location.population;
      temp_row.heatIndex = location.heatIndex;
      temp_row.location = {
        type: "Point",
        coordinates: [location.coordinates[0].lng, location.coordinates[0].lat],
      };
      temp_Array.push({ ...temp_row });
    }
    console.log(temp_Array);
    await db.collection("accessibility_heatmap").insertMany([...temp_Array]);
    completed += temp_Array.length;
    temp_Array = [];
    console.log({ completed });
  }
  console.log("Completed");
});
