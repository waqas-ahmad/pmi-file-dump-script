const lahore = require("./data/lahore/accessibility_index.json");
const islamabad = require("./data/islamabad/accessibility_index.json");
const karachi = require("./data/karachi/accessibility_index.json");
const ULA_ALL = { ...lahore, ...islamabad, ...karachi };

const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://pmi-admin-user:CRDbFpQDzzP2sSlu@pmi-cluster.cnhta.mongodb.net/pmi-development?retryWrites=true&w=majority"
);
let db = mongoose.connection;
mongoose.set("debug", false);
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async () => {
  // await db.collection("ula_all").insertMany([...temp_Array]);
  let completed = 0;
  let temp_Array = [];
  for (const city in ULA_ALL) {
    const selected_city_locations = ULA_ALL[city].locations;
    for (const location of selected_city_locations) {
      let temp_row = {};
      temp_row.city = city;
      temp_row.tam_id = location._id;
      temp_row.address = location.data.address;
      temp_row.population = location.data.population;
      temp_row.flag = location.data.flag;
      temp_row.center = {
        type: "Point",
        coordinates: [
          location.center.marker.position.lng,
          location.center.marker.position.lat,
        ],
      };
      temp_row.polygon = {
        type: "Polygon",
        coordinates: [
          location.boundary.polygon.map(({ lng, lat }) => [lng, lat]),
        ],
      };
      temp_Array.push({ ...temp_row });
    }
    await db.collection("accessibility_polygons").insertMany([...temp_Array]);
    completed += temp_Array.length;
    temp_Array = [];
    console.log({ completed });
  }
  console.log("Completed");
});
